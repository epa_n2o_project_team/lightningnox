#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Build LTNG_2D_DATA (aka 'L2D', which builds lightning NOx parameters files) from sources in my repo=https://bitbucket.org/epa_n2o_project_team/lightningnox
### Details? see https://bitbucket.org/epa_n2o_project_team/lightningnox (in browser, forwards to its README)

### NOTE for HPCC: can currently only build on infinity (not, e.g., amad1) since this ultimately depends (via *config.cmaq*) on libraries in (not my) /home

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### TODO: non-positional argument parsing!

THIS="${0}"
PROJECT_DIR="${1}"      # path to local repository (probably == $THIS_DIR)
L2D_FP="${2}"           # path to L2D ... after we build it!
L2D_MAKEFILE_FP="${3}"  # path to Makefile used to build L2D
UBER_CONFIG_FP="${4}"   # path to CMAQ über-config (sets $M3HOME, etc)
CONFIG_FP="${5}"        # path to CMAQ config (e.g., config.cmaq)
BUILD_BUILDER_FP="${6}" # path to log to write

# ## for testing
# THIS='/project/inf35w/roche/lightning/build_lNOx_builder.sh'
# PROJECT_DIR='/project/inf35w/roche/lightning'
# L2D_FP='/project/inf35w/roche/lightning/LTNG_2D_DATA'
# L2D_MAKEFILE_FP='/project/inf35w/roche/lightning/src/Makefile.LTNG_2D_DATA'
# UBER_CONFIG_FP='/project/inf35w/roche/CMAQ-5.0.1/git/old/CMAQ-build/uber.config.cmaq.sh'
# CONFIG_FP='/project/inf35w/roche/CMAQ-5.0.1/git/old/CMAQ-build/config.cmaq.sh'
# ## end testing

### For logging

THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

function check_prereqs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Test passed args, fail fast on error
  if   [[ -z "${BUILD_BUILDER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} BUILD_BUILDER_FP not defined, exiting ..."
    exit 1
  elif [[ -r "${BUILD_BUILDER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} found log='${BUILD_BUILDER_FP}'--move or delete before proceeding. Exiting ...\n"
    exit 2
  elif [[ -z "${PROJECT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} PROJECT_DIR not defined, exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 3
  elif [[ ! -d "${PROJECT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot find project dir='${PROJECT_DIR}', exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 4
  elif [[ -z "${L2D_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} L2D_FP not defined, exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 5
  elif [[ -r "${L2D_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} found executable='${L2D_FP}'--move or delete before proceeding. Exiting ...\n"
    exit 6
  elif [[ -z "${L2D_MAKEFILE_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} L2D_MAKEFILE_FP not defined, exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 7
  elif [[ ! -r "${L2D_MAKEFILE_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read Makefile='${L2D_MAKEFILE_FP}', exiting ...\n"
    exit 8
  elif [[ -z "${UBER_CONFIG_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} UBER_CONFIG_FP not defined, exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 9
  elif [[ ! -r "${UBER_CONFIG_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read über-config file='${UBER_CONFIG_FP}', exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 10
  elif [[ -z "${CONFIG_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} CONFIG_FP not defined, exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 11
  elif [[ ! -r "${CONFIG_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read config file='${CONFIG_FP}', exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 12
  fi

  ### Parsed from passed args
  export L2D_FN="$(basename ${L2D_FP})"
  export L2D_MAKEFILE_FN="$(basename ${L2D_MAKEFILE_FP})"
  export L2D_MAKEFILE_DIR="$(dirname ${L2D_MAKEFILE_FP})"
  ### ... and test them
  if   [[ -z "${L2D_FN}" ]] ; then
    echo -e "${ERROR_PREFIX} L2D_FN not defined, exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 13
  elif [[ -z "${L2D_MAKEFILE_FN}" ]] ; then
    echo -e "${ERROR_PREFIX} L2D_MAKEFILE_FN not defined, exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 14
  elif [[ -z "${L2D_MAKEFILE_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} L2D_MAKEFILE_DIR not defined, exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 15
  elif [[ ! -d "${L2D_MAKEFILE_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} L2D_MAKEFILE_DIR not defined, exiting ..." 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 16
  fi

} # function check_prereqs

function build_executable {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  # Note '!' below: bash syntax for double dereference/indirection
  for VAR_NAME in 'IOAPI' 'NETCDF' 'FC' 'FC_FLAGS' ; do
    VAR_VAL="${!VAR_NAME}"
    echo -e "${MESSAGE_PREFIX} ${VAR_NAME}='${VAR_VAL}'" 2>&1 | tee -a "${BUILD_BUILDER_FP}"
  done

  # Override makefile settings. Unfortunately just `export`ing is not enough, so ...
  L2D_MAKEFILE_OVERRIDES=''

  if [[ -n "${FC}" ]] ; then
    L2D_MAKEFILE_OVERRIDES="${L2D_MAKEFILE_OVERRIDES} FC='${FC}'"
  fi

  if [[ -n "${FC_FLAGS}" ]] ; then
    L2D_MAKEFILE_OVERRIDES="${L2D_MAKEFILE_OVERRIDES} FC_FLAGS='${FC_FLAGS}'"
  fi

  if [[ -n "${IOAPI_DIR}" ]] ; then
    L2D_MAKEFILE_OVERRIDES="${L2D_MAKEFILE_OVERRIDES} IOAPI='${IOAPI_DIR}'"
  fi

  if [[ -n "${NETCDF_DIR}" ]] ; then
    L2D_MAKEFILE_OVERRIDES="${L2D_MAKEFILE_OVERRIDES} NETCDF='${NETCDF_DIR}'"
  fi

#   if [[ -n "${IOAPI}" && -n "${NETCDF}" ]] ; then
#     LIBRARIES="${IOAPI} ${NETCDF}"
#     L2D_MAKEFILE_OVERRIDES="${L2D_MAKEFILE_OVERRIDES} LIBRARIES='${LIBRARIES}'"
#   fi

#    "make ${L2D_MAKEFILE_OVERRIDES} -f ${L2D_MAKEFILE_FP}" \
#    "mv ${L2D_MAKEFILE_DIR}/${L2D_FN} ${L2D_FP}" \
  for CMD in \
    "pushd ${L2D_MAKEFILE_DIR} ; make ${L2D_MAKEFILE_OVERRIDES} -f ./${L2D_MAKEFILE_FN}" \
    "mv ${L2D_MAKEFILE_DIR}/${L2D_FN} ${L2D_FP}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n"
      exit 17
    fi
  done
} # function build_executable

function check_results {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#    "find ${PROJECT_DIR}/ | fgrep -ve '/.git' | sort" \
  for CMD in \
    "ls -al ${L2D_FP}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n"
      exit 18
    fi
  echo # newline
  done
} # function check_results

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### ASSERT: repo already cloned
for CMD in \
  'check_prereqs' \
; do
#  echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${BUILD_BUILDER_FP}"
  # No, we check for previous log
  echo -e "${MESSAGE_PREFIX} ${CMD}"
#  eval "${CMD}" 2>&1 | tee -a "${BUILD_BUILDER_FP}"
#  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
  # No, we hafta set some envvars in check_prereqs
  eval "${CMD}"
  if [[ "$?" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 19
  fi
  echo # newline
done

### ASSERT: tested these paths above
for FP in \
  "${UBER_CONFIG_FP}" \
  "${CONFIG_FP}" \
; do
  echo -e "${MESSAGE_PREFIX} source ${FP}" 2>&1 | tee -a "${BUILD_BUILDER_FP}"
#  source "${FP}" 2>&1 | tee -a "${BUILD_BUILDER_FP}"
  # No, we *definitely* hafta set some config envvars
  source "${FP}"
  if [[ "$?" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 20
  fi
  echo # newline
done

### Build it (hopefully :-)
for CMD in \
  'build_executable' \
  'check_results' \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${BUILD_BUILDER_FP}"
  eval "${CMD}" 2>&1 | tee -a "${BUILD_BUILDER_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${BUILD_BUILDER_FP}"
    exit 21
  fi
  echo # newline
done

exit 0
