#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Download a URI ($1) and save to a filepath ($2), probably as driven by a lightningnox/Makefile.*
### Details? see https://bitbucket.org/epa_n2o_project_team/lightningnox (in browser, forwards to its README)

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

THIS_FP="$0"
URI="$1"
FP="$2"

### For logging
THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))" # FQ/absolute path
THIS_FN="$(basename ${THIS_FP})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
U2F_LOG_FP="${THIS_DIR}/${THIS_FN}.log"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

### Check arguments.

if   [[ -z "${U2F_LOG_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} U2F_LOG_FP not defined, exiting ..."
  exit 1
elif [[ -z "${URI}" ]] ; then
  echo -e "${ERROR_PREFIX} URI not defined, exiting ..." 2>&1 | tee -a "${U2F_LOG_FP}"
  exit 2
elif [[ -z "${FP}" ]] ; then
  echo -e "${ERROR_PREFIX} FP not defined, exiting ..." 2>&1 | tee -a "${U2F_LOG_FP}"
  exit 3
## Don't fail if file can be read--it might be a partial.
elif [[ -r "${FP}" && ! -w "${FP}" ]] ; then
  echo -e "${ERROR_PREFIX} file='${FP}' can be read but not written, exiting ..." 2>&1 | tee -a "${U2F_LOG_FP}"
  exit 4
fi

### Ensure dir/folder for output

DIR="$(dirname ${FP})"
if   [[ -z "${DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} DIR not defined, exiting ..." 2>&1 | tee -a "${U2F_LOG_FP}"
  exit 5
elif [[ ! -d "${DIR}" ]] ; then
  for CMD in \
    "mkdir -p ${DIR}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${U2F_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${U2F_LOG_FP}"
    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${U2F_LOG_FP}"
      exit 6
    fi
  done

  # sooo ... did it work?
  if [[ ! -d "${DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} could not create plot output dir='${DIR}', exiting ..." 2>&1 | tee -a "${U2F_LOG_FP}"
    exit 7
  fi
fi # [[ ! -d "${DIR}" ]]

### Get the URI using `wget` (or `curl`, but `wget` handles redirects better)

for CMD in \
  "wget -c -O ${FP} ${URI}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${U2F_LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${U2F_LOG_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${U2F_LOG_FP}"
    exit 8
  fi
done

if [[ -r "${FP}" ]] ; then
  for CMD in \
    "ls -al ${FP}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${U2F_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${U2F_LOG_FP}"
    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${U2F_LOG_FP}"
      exit 9
    fi
  done
else
  echo -e "${ERROR_PREFIX} failed to download file='${FP}', exiting ..." 2>&1 | tee -a "${U2F_LOG_FP}"
  exit 10
fi

exit 0
