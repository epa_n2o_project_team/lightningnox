#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Use LTNG_2D_DATA (aka 'L2D') to build lightning NOx (lNOx) parameters files (as discussed @
### http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#Description_7
### ) for CMAQ version > 5. For details regarding building and running L2D, and for its sources, see
### https://bitbucket.org/epa_n2o_project_team/lightningnox

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### TODO: non-positional argument parsing!

THIS="${0}"
METCRO2D_FP="${1}"
ICCG_FP="${2}"
NLDN_MONTHLY_FP="${3}"
MASK_FP="${4}"
LNOX_OUT_FP="${5}"
STRIKE_FACTOR="${6}"
MOLES_N_CG="${7}"
MOLES_N_IC="${8}"
L2D_EXEC_FP="${9}"

### For logging

THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
MAKE_LNOX_LOG_FP="${THIS_DIR}/${THIS_FN}.log"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

### Test args, fail fast on error

if   [[ -z "${MAKE_LNOX_LOG_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} MAKE_LNOX_LOG_FP not defined, exiting ..."
  exit 1
elif [[ -z "${METCRO2D_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} METCRO2D_FP not defined, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 2
elif [[ ! -r "${METCRO2D_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} cannot read grid input='${METCRO2D_FP}', exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 3
elif [[ -z "${ICCG_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} ICCG_FP not defined, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 4
elif [[ ! -r "${ICCG_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} cannot read ICCG input='${ICCG_FP}', exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 5
elif [[ -z "${NLDN_MONTHLY_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} NLDN_MONTHLY_FP not defined, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 6
elif [[ -r "${NLDN_MONTHLY_FP}" && ! -w "${NLDN_MONTHLY_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} NLDN monthly input='${NLDN_MONTHLY_FP}' can be read but not written, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 7
elif [[ -z "${MASK_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} MASK_FP not defined, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 8
elif [[ -r "${MASK_FP}" && ! -w "${MASK_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} mask file='${MASK_FP}' can be read but not written, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 9
elif [[ -z "${LNOX_OUT_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} LNOX_OUT_FP not defined, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 10
elif [[ -r "${LNOX_OUT_FP}" && ! -w "${LNOX_OUT_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} lightning NOx parameters output='${LNOX_OUT_FP}' can be read but not written, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 11
elif [[ -z "${STRIKE_FACTOR}" ]] ; then
  echo -e "${ERROR_PREFIX} STRIKE_FACTOR not defined, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 12
# alas, bash '(( ... ))' tests only work for integers (at least with the versions I'm using) ... but perhaps someday ...
# elif (( ${STRIKE_FACTOR} <= 0 )) ; then
#   echo -e "${ERROR_PREFIX} strike factor='${STRIKE_FACTOR}' <= 0, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
#   exit 13
elif [[ -z "${MOLES_N_CG}" ]] ; then
  echo -e "${ERROR_PREFIX} MOLES_N_CG not defined, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 14
# elif (( ${MOLES_N_CG} <= 0 )) ; then
#   echo -e "${ERROR_PREFIX} cloud-to-ground moles(N)='${MOLES_N_CG}' <= 0, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
#   exit 15
elif [[ -z "${MOLES_N_IC}" ]] ; then
  echo -e "${ERROR_PREFIX} MOLES_N_IC not defined, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 16
# elif (( ${MOLES_N_IC} <= 0 )) ; then
#   echo -e "${ERROR_PREFIX} inter-cloud moles(N)='${MOLES_N_IC}' <= 0, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
#   exit 17
elif [[ -z "${L2D_EXEC_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} L2D_EXEC_FP not defined, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 18
elif [[ ! -r "${L2D_EXEC_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read lightning NOx executable='${L2D_EXEC_FP}', exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 19
fi # [[ -z "${MAKE_LNOX_LOG_FP}" ]]

# ### Common code with run_make_ICCG.sh et al. TODO: refactor!

# ### Handle (broken/downlevel R package=raster || AMAD filenaming conventions):
# ### if METCRO2D_FP does not have a standard extension, *copy* to one that does--
# ### note symlinking is not good enough for package=raster!
# METCRO2D_FN="$(basename ${METCRO2D_FP})"
# METCRO2D_EXT="${METCRO2D_FN##*.}"
# if   [[ -z "${METCRO2D_EXT}" ]] ; then
#   echo -e "${ERROR_PREFIX} METCRO2D_EXT not defined, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
#   exit 20
# # note: 'nc' is the only standard extension per http://www.unidata.ucar.edu/software/netcdf/docs/faq.html#filename
# elif [[ "${METCRO2D_EXT}" != 'nc' ]] ; then

#   METCRO2D_FP_OLD="${METCRO2D_FP}"
#   METCRO2D_FP="${METCRO2D_FP}.nc"
#   echo # newline
#   for CMD in "cp ${METCRO2D_FP_OLD} ${METCRO2D_FP}" ; do
#     echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
#     eval "${CMD}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
#   #  if [[ $? -ne 0 ]] ; then
#     # $? only works for end of pipeline: use PIPESTATUS array to get exit status of each pipeline command
#     if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
#       echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
#       exit 21
#     fi
#     # TODO: check pipeline output from CMD
#   done
#   echo # newline

# fi # [[ -z "${METCRO2D_EXT}" ]]

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### Ensure dir/folder for output

LNOX_PARMS_DIR="$(dirname ${LNOX_OUT_FP})"
if   [[ -z "${LNOX_PARMS_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} LNOX_PARMS_DIR not defined, exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  exit 22
elif [[ ! -d "${LNOX_PARMS_DIR}" ]] ; then
  for CMD in \
    "mkdir -p ${LNOX_PARMS_DIR}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
      exit 23
    fi
  done

  # sooo ... did it work?
  if [[ ! -d "${LNOX_PARMS_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} could not create plot output dir='${LNOX_PARMS_DIR}', exiting ..." 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
    exit 24
  fi
fi # [[ ! -d "${LNOX_PARMS_DIR}" ]]

### Run lightning NOx executable to build parameters file (output).
### Note lightning NOx executable takes arguments via envvars. TODO: fix!

export ICCG="${ICCG_FP}"
export METFILE="${METCRO2D_FP}"
export NLDNFILE="${NLDN_MONTHLY_FP}"
export OCEANMASKFILE="${MASK_FP}"
export OUTFILE="${LNOX_OUT_FP}"
export MOLES_N_CG="${MOLES_N_CG}"
export MOLES_N_IC="${MOLES_N_IC}"
export STRIKE_FACTOR="${STRIKE_FACTOR}"

for CMD in \
  "${L2D_EXEC_FP}" \
; do
  echo -e "${MESSAGE_PREFIX} about to run '${CMD}' with" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  echo -e "\tICCG         =${ICCG}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  echo -e "\tMETFILE      =${METFILE}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  echo -e "\tNLDNFILE     =${NLDNFILE}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  echo -e "\tOCEANMASKFILE=${OCEANMASKFILE}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  echo -e "\tOUTFILE      =${OUTFILE}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  echo -e "\tMOLES_N_CG   =${MOLES_N_CG}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  echo -e "\tMOLES_N_IC   =${MOLES_N_IC}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  echo -e "\tSTRIKE_FACTOR=${STRIKE_FACTOR}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
    exit 25
  fi
done

for CMD in \
  "ls -alh ${OUTFILE}" \
; do
  echo -e "${MESSAGE_PREFIX} about to run '${CMD}'" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${MAKE_LNOX_LOG_FP}"
    exit 25
  fi
done

# ### ASSERT: repo already cloned

# #  'check_prereqs' \  # check prerequisite dir/folders
# #  'get_inputs' \     # get needed monthly inputs, check existence (before we try to ...
# #  'gen_outputs' \    # ... generate outputs.
# #  'check_results' \  # you guessed :-)
# for CMD in \
#   'check_prereqs' \
#   'get_inputs' \
#   'gen_outputs' \
#   'check_results' \
# ; do
#   if [[ -z "${CMD}" ]] ; then
#     echo -e "${THIS_FN}::main loop: ERROR: CMD not defined, exiting ...\n" 2>&1 | tee -a "${LOG_FP}"
#     exit 26
#   else
#     echo -e "${THIS_FN}::main loop::${CMD}\n"
#     eval "${CMD}"
#     if [[ $? -ne 0 ]] ; then
#       echo -e "${THIS_FN}::main loop: ERROR: '${CMD}' failed or not found, exiting ...\n"
#       exit 27
#     fi
#   fi # if [[ -z "${CMD}" ]]
# done

exit 0
