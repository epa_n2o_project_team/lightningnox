#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Call R plot helper=plot_lNOx_parameters.R to ... plot lightning NOx parameters files.
### Note R will require packages={fields, maps, M3}.

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### TODO: non-positional argument parsing!

THIS="$0"
LNOX_PARMS_FP="$1"
METCRO2D_LEN_UNIT="$2"
LNOX_PARMS_PLOT_FP="$3"
PLOT_YYYYMM="$4"
R_HELPER_DIR="$5"
R_RUNNER="$6"
LNOX_PARMS_PLOT_R="$7"

### For logging.

THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
LNOX_PARMS_LOG_FP="${THIS_DIR}/${THIS_FN}.log"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

### Test args, fail fast on error

if   [[ -z "${LNOX_PARMS_LOG_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} LNOX_PARMS_LOG_FP not defined, exiting ..."
  exit 1
elif [[ -z "${LNOX_PARMS_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} LNOX_PARMS_FP not defined, exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 2
elif [[ ! -r "${LNOX_PARMS_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} cannot read NLDN monthly='${LNOX_PARMS_FP}' so cannot plot, exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 3
elif [[ -z "${METCRO2D_LEN_UNIT}" ]] ; then
  echo -e "${ERROR_PREFIX} METCRO2D_LEN_UNIT not defined, exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 4
elif [[ -z "${LNOX_PARMS_PLOT_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} LNOX_PARMS_PLOT_FP not defined, exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 5
elif [[ -r "${LNOX_PARMS_PLOT_FP}" && ! -w "${LNOX_PARMS_PLOT_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} ICCG plot='${LNOX_PARMS_PLOT_FP}' can be read but not written, exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 6
elif [[ -z "${PLOT_YYYYMM}" ]] ; then
  echo -e "${ERROR_PREFIX} PLOT_YYYYMM not defined, exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 7
elif [[ -z "${R_HELPER_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} R_HELPER_DIR not defined, exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 8
elif [[ ! -d "${R_HELPER_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} R helpers dir='${R_HELPER_DIR}' is not a directory, exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 9
elif [[ -z "${R_RUNNER}" ]] ; then
  echo -e "${ERROR_PREFIX} R_RUNNER not defined, exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 10
elif [[ -z "${LNOX_PARMS_PLOT_R}" ]] ; then
  echo -e "${ERROR_PREFIX} LNOX_PARMS_PLOT_R not defined, exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 11
elif [[ ! -r "${LNOX_PARMS_PLOT_R}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read main R file='${LNOX_PARMS_PLOT_R}', exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 12
fi # [[ -z "${LNOX_PARMS_LOG_FP}" ]]

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### Ensure output dir for plot file

LNOX_PARMS_PLOT_DIR="$(dirname ${LNOX_PARMS_PLOT_FP})"
if   [[ -z "${LNOX_PARMS_PLOT_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} LNOX_PARMS_PLOT_DIR not defined, exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  exit 13
elif [[ ! -d "${LNOX_PARMS_PLOT_DIR}" ]] ; then
  for CMD in \
    "mkdir -p ${LNOX_PARMS_PLOT_DIR}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
      exit 14
    fi
  done

  # sooo ... did it work?
  if [[ ! -d "${LNOX_PARMS_PLOT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} could not create plot output dir='${LNOX_PARMS_PLOT_DIR}', exiting ..." 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
    exit 15
  fi
fi # [[ ! -d "${LNOX_PARMS_PLOT_DIR}" ]]

### Run main R script. Note order of arguments: until our R arg-parsing improves, must keep this order.

LNOX_PARMS_START="$(date)"
echo -e "plotting lightning NOx parameters, start=${LNOX_PARMS_START}" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"

for CMD in \
  "${R_RUNNER} ${LNOX_PARMS_PLOT_R}\
   plot_in_fp='${LNOX_PARMS_FP}'\
   METCRO2D_len_unit='${METCRO2D_LEN_UNIT}'\
   plot_out_fp='${LNOX_PARMS_PLOT_FP}'\
   plot_YYYMM='${PLOT_YYYYMM}'\
   our_R_dir='${R_HELPER_DIR}' \
   this_fn='$(basename ${LNOX_PARMS_PLOT_R})'" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
    exit 13
  fi
  # TODO: check pipeline output from CMD
done
echo # newline

### Cleanup, lookaround

for CMD in \
  "ls -al ${LNOX_PARMS_FP}" \
  "ls -al ${LNOX_PARMS_PLOT_FP}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
    exit 15
  fi
done
echo # newline

echo -e "plotting lightning NOx parameters,   end=$(date)" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"
echo -e "plotting lightning NOx parameters, start=${LNOX_PARMS_START}" 2>&1 | tee -a "${LNOX_PARMS_LOG_FP}"

exit 0
