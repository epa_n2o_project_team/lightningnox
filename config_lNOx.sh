#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Depends on
### * `bash` new enough to support
### ** arrays (integer-index, not yet associative)
### ** string manipulations
### * `sed`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### For logging

THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
THIS_LOG_FP="${THIS_DIR}/${THIS_FN}.log"

### Periods over which to loop, in an array.
### Currently supported == MM/YYYY , but feel free to roll your own.
### We will create one of each subsequent artifact for each period, or die trying.

### For the tarball lightning NOx "benchmark" use these temporalities:
# MMYYYY_ARR=( '06/2006' )
### For AQMEII-NA use these temporalities (first is spinup):
MMYYYY_ARR=(\
'12/2007' \
'01/2008' \
'02/2008' \
'03/2008' \
'04/2008' \
'05/2008' \
'06/2008' \
'07/2008' \
'08/2008' \
'09/2008' \
'10/2008' \
'11/2008' \
'12/2008' )

### Makefile configuration.

PROJECT_DIR="${THIS_DIR}"               # Root of repository containing $THIS

MAKEFILE_DIR="${PROJECT_DIR}/Makefiles" # Where to find/put Makefile's?

MAKEFILE_TEMPLATE_FP="${MAKEFILE_DIR}/Makefile.template" # Path to template Makefile for building this project

DEFAULT_MAKEFILE_TARGET='all'           # Makefile target to run.

## config_lNOx.sh will "fill out"

# * temporalities from MMYYYY_ARR, doing the following simple replacements (or those you might care to add). For details, see
# ** https://bitbucket.org/epa_n2o_project_team/lightningnox
# ** https://bitbucket.org/epa_n2o_project_team/lightningnox/raw/HEAD/Makefiles/Makefile.template
YYYYMM_TEMPLATE='@YYYYMM@'
YYYY_TEMPLATE='@YYYY@'
YY_TEMPLATE='@YY@'
MM_TEMPLATE='@MM@'
DD_TEMPLATE='@DD@'

# * the temporality-appropriate ICCG filename.

ICCG_FN_TEMPLATE='@ICCG_FN@'

#   Currently we have only 2 ICCG inputs, summer and winter, so must code the mapping 'temporality -> ICCG filename'.
#   If we had access to newer `bash`, which supports associative arrays, I'd use that
#   but the platforms on which I currently run don't, so I'll fake it.
#   Integer index to following array == month (except 0, which fails).
#   You may need to change this mapping for your spatiotemporality.

MONTH_TO_ICCG_FN_ARR=(\
 'FAIL' \
 'iccg.Boccippio.winter.txt' \
 'iccg.Boccippio.winter.txt' \
 'iccg.Boccippio.winter.txt' \
 'iccg.Boccippio.summer.txt' \
 'iccg.Boccippio.summer.txt' \
 'iccg.Boccippio.summer.txt' \
 'iccg.Boccippio.summer.txt' \
 'iccg.Boccippio.summer.txt' \
 'iccg.Boccippio.summer.txt' \
 'iccg.Boccippio.winter.txt' \
 'iccg.Boccippio.winter.txt' \
 'iccg.Boccippio.winter.txt' )

## Template for path to Makefile for building the current temporality.
## Used below in function=get_Makefile_fp
MAKEFILE_FP_TEMPLATE="${MAKEFILE_DIR}/Makefile.@YYYY@.@MM@"

### METCRO2D configuration.
### We'll need to find a suitable (actually, first matching) METCRO2D file for the given temporality in function=get_day_of_month which uses the following constants.
### Note that Makefile.template knows this also: same information is encoded in its METCRO2D_FP , must match!

## Where to find METCRO2D files
METCRO2D_DIR='/project/inf35w/roche/met/MCIP_v3.6/WRF_2008_24aL/12US1/mcip_out'

## Template for METCRO2D filenames
METCRO2D_FN_TEMPLATE="METCRO2D_${YY_TEMPLATE}${MM_TEMPLATE}${DD_TEMPLATE}"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

### Check constants above for sanity.
function check_arguments {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${THIS_LOG_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} THIS_LOG_FP not defined, exiting ..."
    exit 1
  elif [[ -r "${THIS_LOG_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} previous log='${THIS_LOG_FP}' found--move or delete before proceeding. Exiting ..."
    exit 2
  elif [[ -z "${MMYYYY_ARR}" ]] ; then
    echo -e "${ERROR_PREFIX} MMYYYY_ARR not defined, exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 3
  elif [[ -z "${MAKEFILE_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} MAKEFILE_DIR not defined, exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 4
  elif [[ ! -d "${MAKEFILE_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read Makefile dir/folder='${MAKEFILE_DIR}', exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 5
  elif [[ -z "${MAKEFILE_TEMPLATE_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} MAKEFILE_TEMPLATE_FP not defined, exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 6
  elif [[ ! -r "${MAKEFILE_TEMPLATE_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read Makefile template='${MAKEFILE_TEMPLATE_FP}', exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 7
  fi

#   # start debugging
#   echo -e "${MESSAGE_PREFIX} MMYYYY_ARR ->"
#   # better to loop, but
#   echo -e "${MMYYYY_ARR[@]}"
#   echo -e "${MESSAGE_PREFIX} length(MMYYYY_ARR)=='${#MMYYYY_ARR[@]}'"
#   #   end debugging

} # function check_arguments

### Parse a buncha useful date-like bits from the passed string arg.
function parse_MMYYYY {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  local MMYYYY="$1"
  if   [[ -z "${MMYYYY}" ]] ; then
    echo -e "${ERROR_PREFIX} MMYYYY not defined, exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 8
  fi # if   [[ -z "${MMYYYY}" ]]

  ### Create a buncha useful date-like bits ... in global namespace :-(

  MM="${MMYYYY%/*}"
  if   [[ -z "${MM}" ]] ; then
    echo -e "${ERROR_PREFIX} could not parse MM from '${MMYYYY}', exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 9
  fi
  local MM_LEN="${#MM}"
  if   (( MM_LEN == 1 )) ; then
    MM="$(printf '%02i' ${MM})"
  elif (( MM_LEN > 2 )) ; then
    echo -e "${ERROR_PREFIX} length('${MM}') > 2 from '${MMYYYY}', exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 10
  fi # if   (( MM_LEN == 1 ))

  ### Get ICCG filename corresponding to MM.
  local M="${MM#0*}" # strip any leading zero (but not trailing zero)
  ICCG_FN="${MONTH_TO_ICCG_FN_ARR[${M}]}"
  if [[ -z "${ICCG_FN}" || "${ICCG_FN}" == 'FAIL' ]] ; then
    echo -e "${ERROR_PREFIX} could not get ICCG_FN for month='${M}'=='${MM}', exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 9
  fi

  YYYY="${MMYYYY#*/}"
  if   [[ -z "${YYYY}" ]] ; then
    echo -e "${ERROR_PREFIX} could not parse YYYY from '${MMYYYY}', exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 11
  fi
  local YYYY_LEN="${#YYYY}"
  if   (( YYYY_LEN != 4 )) ; then
    echo -e "${ERROR_PREFIX} length('${YYYY}') != 4 from '${MMYYYY}', exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 12
  fi
  YYYYMM="${YYYY}${MM}"

  YY="${YYYY:2}"
  if   [[ -z "${YY}" ]] ; then
    echo -e "${ERROR_PREFIX} could not parse YY from '${MMYYYY}', exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 13
  fi
  local YY_LEN="${#YY}"
  if   (( YY_LEN != 2 )) ; then
    echo -e "${ERROR_PREFIX} length('${YY}') != 2 from '${MMYYYY}', exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 14
  fi

  ### Get day (as DD) to use for current temporality.
  ### Not needed for tarball benchmark
  DD="$(get_day_of_month)"
  if   [[ -z "${DD}" ]] ; then
    echo -e "${ERROR_PREFIX} DD not defined, exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 15
  elif (( ${DD} == 0 )) ; then
    echo -e "${ERROR_PREFIX} failed to find METCRO2D file for period='${YYYYMM}', exiting ..." 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 16
  fi

  # # start debugging
  # echo -e "${MESSAGE_PREFIX} '${MMYYYY}' ->" 2>&1 | tee -a "${THIS_LOG_FP}"
  # echo -e "\tYYYYMM='${YYYYMM}'" 2>&1 | tee -a "${THIS_LOG_FP}"
  # echo -e "\tYYYY='${YYYY}'" 2>&1 | tee -a "${THIS_LOG_FP}"
  # echo -e "\tYY='${YY}'" 2>&1 | tee -a "${THIS_LOG_FP}"
  # echo -e "\tMM='${MM}'" 2>&1 | tee -a "${THIS_LOG_FP}"
  # echo -e "\tICCG_FN='${ICCG_FN}'" 2>&1 | tee -a "${THIS_LOG_FP}"
  # echo -e "\tDD='${DD}'" 2>&1 | tee -a "${THIS_LOG_FP}"
  # #   end debugging

} # function parse_MMYYYY

### Return first day of month for which there is a matching METCRO2D file.
### Assumes nothing about the suitability of the METCRO2D file found!
function get_day_of_month {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local METCRO2D_FP_PREFIX="${METCRO2D_DIR}/${METCRO2D_FN_TEMPLATE}"
  local METCRO2D_FP_CANDIDATE=''
#  local DD='' # ehh, just set global

  METCRO2D_FP_PREFIX="${METCRO2D_FP_PREFIX/${YY_TEMPLATE}/${YY}}"
  METCRO2D_FP_PREFIX="${METCRO2D_FP_PREFIX/${MM_TEMPLATE}/${MM}}"
  for (( D=1; D<=31; D++ )) ; do
    DD="$(printf '%02d' ${D})"
    METCRO2D_FP_CANDIDATE="${METCRO2D_FP_PREFIX/${DD_TEMPLATE}/${DD}}"
    if [[ -r "${METCRO2D_FP_CANDIDATE}" ]] ; then
      echo -e "${DD}" # return value
      exit 0 # exit this function
    fi
  done
  echo -e '0' # return failure :-(
} # function get_day_of_month

### Create a Makefile path suitable to the current temporality (YYYYMMDD etc).
### Redefine this to make more complex paths.
function get_Makefile_fp {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  MAKEFILE_FP="${MAKEFILE_FP_TEMPLATE/${YYYY_TEMPLATE}/${YYYY}}"
  MAKEFILE_FP="${MAKEFILE_FP/${MM_TEMPLATE}/${MM}}"
  echo -e "${MAKEFILE_FP}" # return value
} # function get_Makefile_fp

### Create a Makefile suitable to the current temporality (YYYYMMDD etc).
function write_Makefile {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Create path to Makefile for current temporality
  MAKEFILE_FP="$(get_Makefile_fp)"

  if   [[ -z "${MAKEFILE_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} MAKEFILE_FP not defined, exiting ..."
    exit 17
  elif [[ -r "${MAKEFILE_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} Makefile='${MAKEFILE_FP}' found--move or delete before proceeding. Exiting ..."
    exit 18
  else

    ## Write new Makefile
    for CMD in \
      "cp ${MAKEFILE_TEMPLATE_FP} ${MAKEFILE_FP}" \
      "sed -i -e 's/${YYYYMM_TEMPLATE}/${YYYYMM}/g'   ${MAKEFILE_FP}" \
      "sed -i -e 's/${YYYY_TEMPLATE}/${YYYY}/g'       ${MAKEFILE_FP}" \
      "sed -i -e 's/${YY_TEMPLATE}/${YY}/g'           ${MAKEFILE_FP}" \
      "sed -i -e 's/${MM_TEMPLATE}/${MM}/g'           ${MAKEFILE_FP}" \
      "sed -i -e 's/${DD_TEMPLATE}/${DD}/g'           ${MAKEFILE_FP}" \
      "sed -i -e 's/${ICCG_FN_TEMPLATE}/${ICCG_FN}/g' ${MAKEFILE_FP}" \
    ; do
      if [[ -z "${CMD}" ]] ; then
	echo -e "${ERROR_PREFIX} CMD not defined, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
	exit 19
      else
	echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
	eval "${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
	if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
	  echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
	  exit 20
	fi
      fi # if [[ -z "${CMD}" ]]
    done # for CMD
  fi # [[ -z "${MAKEFILE_FP}" ]]
} # function write_Makefile

### Run the Makefile created with write_Makefile.
function run_Makefile {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${MAKEFILE_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} MAKEFILE_FP not defined, exiting ..."
    exit 21
  elif [[ -z "${DEFAULT_MAKEFILE_TARGET}" ]] ; then
    echo -e "${ERROR_PREFIX} DEFAULT_MAKEFILE_TARGET not defined, exiting ..."
    exit 22
  elif [[ ! -r "${MAKEFILE_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read Makefile='${MAKEFILE_FP}', exiting ..."
    exit 23
  else
    for CMD in \
      'make ${DEFAULT_MAKEFILE_TARGET} -f ${MAKEFILE_FP}' \
    ; do
      if [[ -z "${CMD}" ]] ; then
	echo -e "${ERROR_PREFIX} CMD not defined, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
	exit 24
      else
	echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
	eval "${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
	if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
	  echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n"
	  exit 25
	fi
      fi # if [[ -z "${CMD}" ]]
    done # for CMD
    echo # newline
  fi
} # function run_Makefile

### What hath run_Makefile wrought?
function check_results {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    "ls -al \$(find ${PROJECT_DIR}/ -name '*.csv')" \
    "ls -al \$(find ${PROJECT_DIR}/ -name '*.ioapi')" \
    "ls -al \$(find ${PROJECT_DIR}/ -name '*.pdf')" \
  ; do
    if [[ -z "${CMD}" ]] ; then
      echo -e "${ERROR_PREFIX} CMD not defined, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
      exit 98
    else
      echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
      if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
        exit 99
      fi
    fi # if [[ -z "${CMD}" ]]
  done # for CMD

} # function check_results

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

MESSAGE_PREFIX="${THIS_FN}::main loop:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

# # start debugging
# echo -e "${MESSAGE_PREFIX} MMYYYY_ARR ->"
# # better to loop, but
# echo -e "${MMYYYY_ARR[@]}"
# echo -e "${MESSAGE_PREFIX} length(MMYYYY_ARR)=='${#MMYYYY_ARR[@]}'"
# #   end debugging

for CMD in \
  'check_arguments' \
; do
  if [[ -z "${CMD}" ]] ; then
    echo -e "${ERROR_PREFIX} CMD not defined, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 98
  else
    ## Don't write log yet: will test for previous.
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
#    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    if [[ "$?" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n"
      exit 99
    fi
  fi # if [[ -z "${CMD}" ]]
done # for CMD
echo # newline

## TODO: get_NLDN_den_URI iff NLDN_DEN_ACCESS='download'
for MMYYYY in "${MMYYYY_ARR[@]}"; do
  for CMD in \
    "parse_MMYYYY ${MMYYYY}" \
    'write_Makefile' \
  ; do
    if [[ -z "${CMD}" ]] ; then
      echo -e "${ERROR_PREFIX} CMD not defined, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
      exit 98
    else
      echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
#       eval "${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
#       if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      # can't `tee` and write envvars!
      eval "${CMD}"
      if [[ "$?" -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
        exit 99
      fi
    fi # if [[ -z "${CMD}" ]]
  done # for CMD

  for CMD in \
    'run_Makefile' \
  ; do
    if [[ -z "${CMD}" ]] ; then
      echo -e "${ERROR_PREFIX} CMD not defined, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
      exit 98
    else
      echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
      if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
        exit 99
      fi
    fi # if [[ -z "${CMD}" ]]
  done # for CMD
  echo # newline
done # for MMYYYY
echo # newline

for CMD in \
  'check_results' \
; do
  if [[ -z "${CMD}" ]] ; then
    echo -e "${ERROR_PREFIX} CMD not defined, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
    exit 98
  else
    echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${THIS_LOG_FP}"
    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${THIS_LOG_FP}"
      exit 99
    fi
  fi # if [[ -z "${CMD}" ]]
done # for CMD
echo # newline

exit 0
