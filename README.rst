*(part of the* `AQMEII-NA_N2O <https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home>`_ *family of projects)*

.. contents:: **Table of Contents**

open-source notice
==================

Copyright 2013, 2014 ``Tom Roche <Tom_Roche@pobox.com>``

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the `GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html>`_ as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the `GNU Affero General Public License <./COPYING>`_ for more details.

.. figure:: ../../downloads/Affero_badge__agplv3-155x51.png
   :scale: 100 %
   :alt: distributed under the GNU Affero General Public License
   :align: left

summary
=======

Code in this repository supports the CMAQ_ workflow= `inline with parameters`_ for users to preprocess lightning-|NOx| observations and related data into a form consumable by CCTM_. Hopefully it will soon migrate to a repository managed by CMAS_ or EPA_.

.. _CCTM: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#CMAQ_Chemistry-Transport_Model_.28CCTM.29
.. _CMAQ: http://www.cmaq-model.org/
.. _CMAS: http://www.cmascenter.org/
.. _EPA: http://www.epa.gov/AMD/
.. _inline with parameters: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/lightning_NOx_over_AQMEII-NA_2008#!modes-of-support
.. |NOx| replace:: NO\ :sub:`x`

details
=======

initial commit
--------------

Provenance of files used for initial commit:

1. I downloaded and expanded the `CMAQ-5.0.1 tarballs`_ as suggested on the `CMAQ wiki <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#Installing_and_Compiling_CMAQ_Source_Code>`_.
2. I defined the environment variables ``M3HOME`` and ``M3DATA`` as directed on the `CMAQ wiki <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#Notes_on_the_CMAQ_directory_structure>`_.
3. I ran the following bash_ session:

   ::

       $ mkdir -p ~/code/lightningNOx/ # new dir/folder, not in tarball tree
       $ pushd ~/code/lightningNOx/
       $ git init
       # if tarsplat on remote host, use `rsync -avh --append`
       $ cp -r ${M3HOME}/scripts/lnox/* ./
       $ rm -fr ./R-out/
       $ rm -fr ./R-scripts/.Rhistory
       $ mv ./README ./README.0
       $ chmod 444 ./README.0
       $ chmod 444 R-scripts/*.dump
       $ chmod 444 R-scripts/README
       # get ICCG inputs: they are small enough to manage in repo
       $ mkdir -p ./ICCG_in/
       $ cp ${M3DATA}/raw/lnox/input/* ./ICCG_in/
       $ chmod 444  ./ICCG_in/*
       # ocean mask, NLDN inputs are too large

.. _CMAQ-5.0.1 tarballs: https://www.cmascenter.org/download/software/cmaq/cmaq_5-0-1.cfm
.. _bash: http://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
.. _Makefiles: http://en.wikipedia.org/wiki/Make_%28software%29#Makefiles
.. _Makefile\.template: ../../src/HEAD/Makefiles/Makefile.template?at=master
.. _config\_lNOx\.sh: ../../src/HEAD/config_lNOx.sh?at=master
.. _make\_lNOx\_parameters.sh: ../../src/HEAD/make_lNOx_parameters.sh?at=master
.. _make\_mask.R: ../../src/HEAD/R-scripts/make_mask.R?at=master

code structure
--------------

The current code generates Makefiles_  to build the various lightning-|NOx| artifacts. The primary generators are

.. Note: link definition/use for links={Makefile.template, config_lNOx.sh} fail unless dots and underscores are backslashed in definition only!

- `Makefile.template`_: this template ``Makefile`` composes ``make`` variables, targets, rules, and recipes that encode all information required to build lightning-|NOx| artifacts for a given temporality (or time period) **except** the temporal information itself, which is present only as template values.
- `config_lNOx.sh`_ contains one or more temporalities over which it iterates, instantiating a ``Makefile`` for each temporality by writing the temporal template values.

The primary generators drive one or more secondary generators for either

-  getting (whether creating or retrieving)
-  plotting

each of the major types of lightning-|NOx| artifacts:

.. _mask files: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/lightning_NOx_over_AQMEII-NA_2008#!mask-csv
.. _MET_CRO_2D file: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/lightning_NOx_over_AQMEII-NA_2008#!met_cro_2d
.. _ICCG files: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/lightning_NOx_over_AQMEII-NA_2008#!iccg-csv
.. _flash-totals files: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/lightning_NOx_over_AQMEII-NA_2008#!flash-totals
.. _NLDN: http://gcmd.nasa.gov/records/GCMD_NLDN.html
.. _flash-parameters files: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/lightning_NOx_over_AQMEII-NA_2008#!flash-parameters
.. _tarballs \"benchmark\": https://www.cmascenter.org/download/software/cmaq/cmaq_5-0-1.cfm

.. Note:
.. * link definition/use for 'tarballs \"benchmark\"' only works if quotes are backslashed in BOTH definition AND use!
.. * placing a comment between list items causes a visible break in the rendered list.

1. `mask files`_. These are spatial artifacts; assuming that lightning-|NOx| is being built for a normal CMAQ run with a single spatiality, only one mask file will need to be built. Building the mask file requires as input a `MET_CRO_2D file`_ (or ``METCRO2D``) file, which is assumed to be provided with the rest of the meteorology for one's CMAQ run. The ``METCRO2D`` file is only accessed for its (IOAPI-provided, horizontal) grid information; since (again) that information is supposed to be constant over a CMAQ run, any ``METCRO2D`` for any temporality in the run should suffice.
2. `ICCG files`_. These are temporal artifacts, but have inputs for only 'summer' and 'winter', so the user must arbitrarily map their temporality to that space (in `config_lNOx.sh`_).
3. `flash-totals files`_. These can be either downloaded or built, however the current code only supports download. (That being said, (Rob Pinder's?) code from the `tarballs \"benchmark\"`_ is in this repo, and could be presumably be recruited to build monthly flash-totals from "raw" `NLDN`_ data if required.)
4. `flash-parameters files`_. These are the current primary output of this project, providing lightning-|NOx| data suitable for input to CCTM_.

.. Note: several attempts (i.e., syntax variants) to reuse anonymous reference==__ CMAQ-5.0.1 tarballs_ above failed.

.. _R: http://en.wikipedia.org/wiki/R_%28programming%20language%29
.. _failing fast: https://en.wikipedia.org/wiki/Fail-fast
.. _LTNG\_2D\_DATA: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#LTNG_2D_DATA
.. _wget: https://en.wikipedia.org/wiki/Wget

Secondary generators are either

- a pair of (`bash`_, `R`_) scripts, s.t. the ``bash`` script takes and processes arguments from ``make`` (`failing fast`_ on error), passing them to the ``R`` script, which does the real work. (This is done because I find argument handling in ``R`` more tedious than argument handling in ``bash``.) This process generates mask and ICCG artifacts, and plots all of the artifacts.
- a ``bash`` script driving `LTNG_2D_DATA`_, to generate `flash-parameters files`_.
- a ``bash`` script driving `wget`_, to download `flash-totals files`_.

The current code also incorporates (Rob Pinder's?) previous code for building `LTNG_2D_DATA`_ (and the ``Makefile`` mechanism ensures that it's only built once).

.. Note: link definition/use for LTNG_2D_DATA fails unless underscores are backslashed ONLY in definition!

use on HPCC
-----------

.. _R package=optparse: http://cran.r-project.org/web/packages/optparse/
.. _R package=rgdal: http://cran.r-project.org/web/packages/rgdal/
.. _R package=raster: http://cran.r-project.org/web/packages/raster/

The repository code is setup to build/run "out of the box" on EPA AMAD HPCC (as of Mar 2014). Unfortunately, HPCC has some annoying quirks:

.. _HPCC usage notes:
.. _HPCC quirks:

1. ``amad1`` has both a {working, not too downlevel}{Intel Fortran, `R`_}, but lacks the necessary links to Dave Wong's magic IOAPI and netCDF libraries.
2. ``infinity`` has the magic libraries and matching Intel Fortran, but its ``R`` is broken WRT `R package=rgdal`_, which breaks `R package=raster`_ (used for regridding)
3. No EPA system (EMVL or HPCC) supports ``ssh``-ing out, and therefore also does not support ``git`` protocol = ``git``.
4. No EPA system of which I'm aware (and definitely not EMVL or HPCC) has "most normal" SSL certificates (and certainly not bitbucket's)

which complicates the build/run process unnecessarily, but not unduly. The build/run process is essentially

.. Note: pandoc correctly autonumbered/rendered the following (which were each '1.'), but BB-reST rendered as a single string (no line breaks).

1. *(application-specific)* Setup a workspace
2. *(application-specific)* Edit `Makefile.template`_
3. *(generic)* Run `config_lNOx.sh`_ in a manner complicated by the above caveats.

In the following subsections, I give

.. Note that ALL internal links/targets, including implicit hyperlinks targeting section titles, FAIL for PDF output from pandoc (which renders them literally), but BB-reST renders them correctly.

1. the application-specific steps for build/run-ing for the `benchmark`_
2. the application-specific steps for build/run-ing for `AQMEII-NA`_
3. the `generic`_/common steps.

.. _AQMEII-NA_N2O study: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home

benchmark
_________

Sample lightning-|NOx| data and reference output are included with the `CMAQ-5.0.1 tarballs`_. I refer to that as the *tarballs "benchmark"* since it's not part of the `actual CMAQ-5.0.1 benchmark <http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#Benchmarking>`_ but serves a similar function. To build it on HPCC,

.. Note nested lists fail without newlines around them!
.. Note backslash in list item=2.1: nested list fails to render without it!
.. Note also that I could not put the previous comment "in position" between list item=2 and list item=2.1: hosed the nested list!

.. _benchmark build instructions:

1. Unpack the `CMAQ-5.0.1 tarballs`_ to a shared space (i.e., on ``/project``), and record the location.
2. Clone branch = ``tarball-benchmark`` of `this repository <http://bitbucket.org/tlroche/lightningnox/>`_ to shared space. Given the `HPCC usage notes`_ above, one must do something like the following (note line broken to accommodate page width)

   ::

      env GIT_SSL_NO_VERIFY=true git clone -b tarball-benchmark \
       https://bitbucket.org/epa_n2o_project_team/lightningnox.git

3. Note the path created by ``git clone``, i.e., the path to the cloned repository.
4. In `Makefile.template`_, edit the paths to

   -  ``PROJECT_DIR``\==path to cloned repository
   -  ``TARBALL_ROOT``\==path to root of unpacked tarballs

5. (necessitated by `HPCC quirks`_) Open `config_lNOx.sh`_ and note the first element in ``MMYYYY_ARR``, which is ``06/2006``. Note that this will cause the build process to die on ``infinity`` after creating ``Makefile.2006.06``, so we will need to delete that file before restarting on ``amad1``.
6. With the previous step in mind, run the `generic`_ steps of the build.

The above process should result in the following outputs:

.. TODO: how to make reST right-justify number columns?

=========================================================================================================================================  =======
path                                                                                                                                       size
=========================================================================================================================================  =======
`$PROJECT_DIR/ICCG_out/iccg.2006.06.csv <../../downloads/iccg.2006.06.csv>`_                          1453750
`$PROJECT_DIR/mask_out/mask.csv <../../downloads/mask.csv>`_                                           363420
`$PROJECT_DIR/LTNG_2D_DATA_out/LTNG_RATIO.2006.06.ioapi <../../downloads/LTNG_RATIO.2006.06.ioapi>`_  4404668
`$PROJECT_DIR/NLDN_in/NLDN.2006.06.IOAPI <ftp://ftp.unc.edu/pub/cmas/DATA/NLDN_CMAQ/2006/NLDN.2006.06.ioapi>`_                              560160
`$PROJECT_DIR/ICCG_out/iccg.2006.06.pdf <../../downloads/iccg.2006.06.pdf>`_                           460445
`$PROJECT_DIR/LTNG_2D_DATA_out/lNOx_parms.2006.06.pdf <../../downloads/lNOx_parms.2006.06.pdf>`_      3636240
`$PROJECT_DIR/mask_out/mask.pdf <../../downloads/mask.pdf>`_                                           448648
`$PROJECT_DIR/NLDN_out/NLDN.2006.06.pdf <../../downloads/NLDN.2006.06.pdf>`_                           346148
=========================================================================================================================================  =======

The tarballs' reference or "known-good" output (including plots) for the "benchmark" should be @ ``$TARBALL_ROOT/CMAQv5.0.1/data/ref/lnox/`` (Note the "benchmark" used PNG output for some plots, while all my plots are PDF.)

AQMEII-NA
_________

.. Note that attempt to make link to section=benchmark above with
.. > building the `benchmark`_,
.. or
.. > building the `benchmark`_\,
.. failed to render correctly with pandoc.

Building lightning-|NOx| for the `AQMEII-NA_N2O study`_ is quite similar to building the benchmark, except that

1. There is no need for the `CMAQ-5.0.1 tarballs`_.
2. Clone project branch = ``master`` instead of branch = ``tarball-benchmark``

The build process for AQMEII-NA is

.. Note that attempt to make link to anchor='HPCC usage notes' above with
.. > Given the `HPCC usage notes`_ above
.. failed to render correctly with pandoc, but I'm gonna try it anyway with BB-rst.

.. _AQMEII-NA build instructions:

1. Clone branch = ``master`` of `this repository <http://bitbucket.org/tlroche/lightningnox/>`_ to shared space. Given the `HPCC usage notes`_ above, one must do something like the following

   ::

      env GIT_SSL_NO_VERIFY=true git clone https://bitbucket.org/epa_n2o_project_team/lightningnox.git

2. Note the path created by ``git clone``, i.e., the path to the cloned repository.
3. In `Makefile.template`_, edit the path to

   -  ``PROJECT_DIR``\==path to cloned repository

4. (necessitated by `HPCC quirks`_) Open `config_lNOx.sh`_ and note the first element in ``MMYYYY_ARR``, which is ``12/2007``. Note that this will cause the build process to die on ``infinity`` after creating ``Makefile.2007.12``, so we will need to delete that file before restarting on ``amad1``.
5. With the previous step in mind, run the `generic`_ steps of the build.

Running the above steps should produce many artifacts. The most important outputs will be in ``$PROJECT_DIR/LTNG_2D_DATA_out`` (unless you change paths in ``Makefile.template``), including the following plots:

- `lNOx_parms.2007.12.pdf <../../downloads/lNOx_parms.2007.12.pdf>`_
- `lNOx_parms.2008.01.pdf <../../downloads/lNOx_parms.2008.01.pdf>`_
- `lNOx_parms.2008.02.pdf <../../downloads/lNOx_parms.2008.02.pdf>`_
- `lNOx_parms.2008.03.pdf <../../downloads/lNOx_parms.2008.03.pdf>`_
- `lNOx_parms.2008.04.pdf <../../downloads/lNOx_parms.2008.04.pdf>`_
- `lNOx_parms.2008.05.pdf <../../downloads/lNOx_parms.2008.05.pdf>`_
- `lNOx_parms.2008.06.pdf <../../downloads/lNOx_parms.2008.06.pdf>`_
- `lNOx_parms.2008.07.pdf <../../downloads/lNOx_parms.2008.07.pdf>`_
- `lNOx_parms.2008.08.pdf <../../downloads/lNOx_parms.2008.08.pdf>`_
- `lNOx_parms.2008.09.pdf <../../downloads/lNOx_parms.2008.09.pdf>`_
- `lNOx_parms.2008.10.pdf <../../downloads/lNOx_parms.2008.10.pdf>`_
- `lNOx_parms.2008.11.pdf <../../downloads/lNOx_parms.2008.11.pdf>`_
- `lNOx_parms.2008.12.pdf <../../downloads/lNOx_parms.2008.12.pdf>`_

generic
_______

Note that the following are complicated by the `HPCC quirks`_ noted above, which hopefully will be fixed Real Soon Now.

1. Open a shell on ``infinity``, and run (filling-in the envvar appropriately)

   -  ``ls -al $PROJECT_DIR/config_lNOx.sh``

2. Presuming that's found, run `config_lNOx.sh`_

   -  ``$PROJECT_DIR/config_lNOx.sh``
   - ... which should build `LTNG_2D_DATA`_, then die when it hits `R`_.

3. Open a shell on ``amad1`` (or other HPCC box with a working ``R`` as defined above), and delete two files created by the previous invocation of ``config_lNOx.sh``:

   -  one is the ``Makefile`` noted in your application-specific setup , e.g., ``rm $PROJECT_DIR/Makefiles/Makefile.2006.06``
   -  the other is common to all applications for this project: ``rm $PROJECT_DIR/config_lNOx.sh.log``

4. In the same ``R``-worthy shell, run (again) ``config_lNOx.sh``:

   -  ``$PROJECT_DIR/config_lNOx.sh``
   -  ... which should build the application-specific artifacts noted above (in each application-specific section)

TODOs
=====

.. _run\_plot\_lNOx\_parameters.sh: ../../src/HEAD/run_plot_lNOx_parameters.sh?at=master
.. _plot\_lNOx\_parameters.R: ../../src/HEAD/R-scripts/plot_lNOx_parameters.R?at=master
.. _make\_ICCG.R: ../../src/HEAD/R-scripts/make_ICCG.R?at=master
.. _plot\_ICCG.R: ../../src/HEAD/R-scripts/plot_ICCG.R?at=master
.. _plot\_mask.R: ../../src/HEAD/R-scripts/plot_mask.R?at=master
.. _plot\_NLDN\_monthly.R: ../../src/HEAD/R-scripts/plot_NLDN_monthly.R?at=master
.. _run\_make\_ICCG.sh: ../../src/HEAD/run_make_ICCG.sh?at=master
.. _run\_plot\_ICCG.sh: ../../src/HEAD/run_plot_ICCG.sh?at=master

1. investigate (ask Pinder?) about data and plot manipulations:

   1. *parameters-file plot*. my `plot <../../downloads/lNOx_parms.2006.06.pdf>`_ of the tarball reference output data (``${TARBALL_ROOT}/CMAQv5.0.1/data/ref/lnox/LTNG_RATIO.2006.06.ioapi``, mounted `here <../../downloads/LTNG_RATIO.2006.06.ioapi>`_) differs from the plot packaged with the tarball reference output (``${TARBALL_ROOT}/CMAQv5.0.1/data/ref/lnox/plot_LNOx_params.pdf``, mounted `here <../../downloads/ref_plot_LNOx_params.pdf>`_) in page 5 (CMAQ vs NLDN strike bias). Note

      - all other pages in that plot (including CMAQ-estimated strikes and NLDN strike obs) match.
      - this is the only data calculated (rather than just being retrieved) in `plot_lNOx_parameters.R`_

   2. *ICCG*. `make_ICCG.R`_ reverses data rows in output to match reference data

      - which gets transposed in `plot_ICCG.R`_ (plotting from CSV)
      - but not in `plot_lNOx_parameters.R`_ (plotting from IOAPI datavar)

   3. *mask*:

      - `reference mask plot <../../downloads/ref_ocean_mask.png>`_ (and presumably data) is either lower-resolution, or deliberately 'continental-shelf's its output relative to `mine <../../downloads/mask.pdf>`_, which much more finely resolves coastlines and islands.
      - `plot_mask.R`_ transposes data (retrieved via R = ``read.csv``) before its call to R = ``image``
      - but `plot_lNOx_parameters.R`_ does not transpose data (retrieved via ``M3::get.M3.var``)
      - neither reverses rows

   4. *NLDN monthly*. I don't know how data was processed by whoever put the files online for download; I do know that `plot_NLDN_monthly.R`_ neither transposes or reverses.
   5. my ``plot_lNOx_parameters.R``: does not reverse rows when plotting the above data

      * ICCG
      * mask
      * NLDN monthly

      but *does* reverse rows when plotting data for

      * CMAQ-calculated strikes
      * strike bias
      * moles(NO)/strike
      * strike count parameter

2. ``MET_CRO_2D`` handling: `run_make_ICCG.sh`_ and `run_plot_ICCG.sh`_ copy the ``MET_CRO_2D`` file locally to ``*.nc`` (i.e., they change the file extension) to make netCDF tools happy. Having our data files use non-standard file extensions is bad, but there's not much I can do about that. What's egregious and I *can* change is, don't copy it more than once!

   * Code should check for the file, and copy only if not found, so as to **only copy this large file once**.
   * ``Makefile``\(s) target= ``clean`` should remove the ``*.nc`` file(s).

3. Move all these TODOs to this project's `issue tracker <../../issues>`_.
4. For all code that uses: document (``uber.config.cmaq.*``, ``config.cmaq*``) from `CMAQ-build <https://bitbucket.org/epa_n2o_project_team/cmaq-build>`_
5. Project needs extensive refactoring! lotsa common code :-(

   - write/use more functions, put in 'source'able files, call from {payload, main loop}
   - note lotsa functions in previous version of `make_lNOx_parameters.sh`_ that should probably be recovered!
   - `R`_ scripts in this project: fix/use common plotting functions, **undo** row-reversal in ``get.*`` functions

.. Note that attempt to anonymous reference 'R scripts' -> target='R' above fails
.. Note that following subscript-including link works! though emacs fails to color it correctly

6. Refactor this and my lightning-|NOx| `wikipage <https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/lightning_NOx_over_AQMEII-NA_2008>`_ which currently share (e.g.) info on running this code.
7. R scripts in this project: need better arg parsing. Currently all use vanilla R ``commandArgs``, which are completely positional. Instead, use `R package=optparse`_: see

   - this `optparse vignette <http://www.icesi.edu.co/CRAN/web/packages/optparse/vignettes/optparse.pdf>`_
   - this `helpful page <http://www.cureffi.org/2014/01/15/running-r-batch-mode-linux/>`_

8. all my bash scripts (this project and beyond):

   - need better arg parsing: use built-in ``getopts`` (though it can't handle long options)
   - if calling a function=$CMD && 'tee-ing eval' (e.g., doing ``eval $CMD 2>&1 | tee -a $LOG_FP``), make sure that $CMD is *not* writing log within its body (producing doubled lines in log)
   - if calling a function=$CMD && not 'tee-ing eval', make sure that $CMD *is* writing log within its body (to ensure completeness of log)

9. `make_mask.R`_: complete vectorization: still loops to calculate weights.
10. all ``plot*.R``: make subtitles plot
11. support workflow=[download `NLDN`_ hourly, use to build NLDN monthly]. Currently I only support downloading NLDN monthly.
12. create {``*_driver.sh``, ``uber_driver.sh``} à la regridders to either {create bare dir for full 'make' testing, test from repo clone}.
