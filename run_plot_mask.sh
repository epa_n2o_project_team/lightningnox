#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Setup and run plot_mask.R (derived from ocean_mask.R).
### Note R will require packages={fields, maps, M3}.
### Should not be necessary when run from ./Makefiles/*, except that
### downlevel R package=raster (e.g., version='2.0-12 (1-September-2012)' on HPCC) don't support netCDF with non-standard file extensions

### Much code in common with run_{make, plot}_{ICCG, mask}.sh. TODO: refactor!

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### TODO: non-positional argument parsing!

THIS="$0"
METCRO2D_FP="$1"
METCRO2D_LEN_UNIT="$2"
MASK_OUT_FP="$3"
MASK_PLOT_FP="$4"
PLOT_YYYYMM="$5"
R_RUNNER="$6"
MASK_PLOT_R="$7"

### For logging

THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
THIS_PREFIX='mask' # "${THIS_FN%.*}" but don't want the 'run_'
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
MASK_LOG_FP="${THIS_DIR}/${THIS_FN}.log"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

### Test args, fail fast on error

if   [[ -z "${MASK_LOG_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} MASK_LOG_FP not defined, exiting ..."
  exit 1
elif [[ -z "${METCRO2D_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} METCRO2D_FP not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 2
elif [[ ! -r "${METCRO2D_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} cannot read grid input='${METCRO2D_FP}', exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 3
elif [[ -z "${METCRO2D_LEN_UNIT}" ]] ; then
  echo -e "${ERROR_PREFIX} METCRO2D_LEN_UNIT not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 4
elif [[ -z "${MASK_OUT_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} MASK_OUT_FP not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 7
elif [[ -r "${MASK_OUT_FP}" && ! -w "${MASK_OUT_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} MASK output='${MASK_OUT_FP}' can be read but not written, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 8
elif [[ -z "${MASK_PLOT_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} MASK_PLOT_FP not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 9
elif [[ -r "${MASK_PLOT_FP}" && ! -w "${MASK_PLOT_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} MASK plot='${MASK_PLOT_FP}' can be read but not written, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 10
elif [[ -z "${PLOT_YYYYMM}" ]] ; then
  echo -e "${ERROR_PREFIX} PLOT_YYYYMM not defined, exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 11
elif [[ -z "${R_RUNNER}" ]] ; then
  echo -e "${ERROR_PREFIX} R_RUNNER not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 12
elif [[ -z "${MASK_PLOT_R}" ]] ; then
  echo -e "${ERROR_PREFIX} MASK_PLOT_R not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 13
elif [[ ! -r "${MASK_PLOT_R}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read main R file='${MASK_PLOT_R}', exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 14
fi # [[ -z "${MASK_LOG_FP}" ]]

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### Run main R script. Note order of arguments: until our R arg-parsing improves, must keep this order.

MASK_START="$(date)"
echo -e "MASK START=${MASK_START}"

for CMD in \
  "${R_RUNNER} ${MASK_PLOT_R}\
   METCRO2D_fp='${METCRO2D_FP}'\
   METCRO2D_len_unit='${METCRO2D_LEN_UNIT}'\
   mask_out_fp='${MASK_OUT_FP}'\
   mask_img_fp='${MASK_PLOT_FP}' \
   plot_YYYYMM='${PLOT_YYYYMM}' \
   this_fn='$(basename ${MASK_PLOT_R})'" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${MASK_LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${MASK_LOG_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${MASK_LOG_FP}"
    exit 16
  fi
  # TODO: check pipeline output from CMD
done
echo # newline

### Cleanup, lookaround

for CMD in \
  "ls -al ${METCRO2D_FP}*" \
  "ls -al ${MASK_OUT_FP}" \
  "ls -al ${MASK_PLOT_FP}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${MASK_LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${MASK_LOG_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${MASK_LOG_FP}"
    exit 17
  fi
done
echo # newline

echo -e "MASK   END=$(date)" 2>&1 | tee -a "${MASK_LOG_FP}"
echo -e "MASK START=${MASK_START}" 2>&1 | tee -a "${MASK_LOG_FP}"

exit 0
