#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Setup and run make_ICCG.R (derived from iccg.R).
### Note R will require packages={M3, raster}.
### Should not be necessary when run from ./Makefiles/*, except that
### downlevel R package=raster (e.g., version='2.0-12 (1-September-2012)' on HPCC) don't support netCDF with non-standard file extensions

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### TODO: non-positional argument parsing!

THIS="$0"
METCRO2D_FP="$1"
METCRO2D_VAR_NAME="$2"
METCRO2D_LEN_UNIT="$3"
ICCG_IN_FP="$4"
ICCG_OUT_FP="$5"
R_RUNNER="$6"
ICCG_BUILD_R="$7"

### For logging

THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
THIS_PREFIX='iccg' # "${THIS_FN%.*}" but don't want the 'run_'
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
ICCG_LOG_FP="${THIS_DIR}/${THIS_FN}.log"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

### Test args, fail fast on error

if   [[ -z "${ICCG_LOG_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} ICCG_LOG_FP not defined, exiting ..."
  exit 1
elif [[ -z "${METCRO2D_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} METCRO2D_FP not defined, exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 2
elif [[ ! -r "${METCRO2D_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} cannot read grid input='${METCRO2D_FP}', exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 3
elif [[ -z "${METCRO2D_LEN_UNIT}" ]] ; then
  echo -e "${ERROR_PREFIX} METCRO2D_LEN_UNIT not defined, exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 4
elif [[ -z "${METCRO2D_VAR_NAME}" ]] ; then
  echo -e "${ERROR_PREFIX} METCRO2D_VAR_NAME not defined, exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 5
elif [[ -z "${ICCG_IN_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} ICCG_IN_FP not defined, exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 6
elif [[ ! -r "${ICCG_IN_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} cannot read ICCG input='${ICCG_IN_FP}', exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 7
elif [[ -z "${ICCG_OUT_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} ICCG_OUT_FP not defined, exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 8
elif [[ -r "${ICCG_OUT_FP}" && ! -w "${ICCG_OUT_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} ICCG output='${ICCG_OUT_FP}' can be read but not written, exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 9
elif [[ -z "${R_RUNNER}" ]] ; then
  echo -e "${ERROR_PREFIX} R_RUNNER not defined, exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 10
elif [[ -z "${ICCG_BUILD_R}" ]] ; then
  echo -e "${ERROR_PREFIX} ICCG_BUILD_R not defined, exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 11
elif [[ ! -r "${ICCG_BUILD_R}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read main R file='${ICCG_BUILD_R}', exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 12
fi # [[ -z "${ICCG_LOG_FP}" ]]

### Ensure dir/folder for output

ICCG_OUT_DIR="$(dirname ${ICCG_OUT_FP})"
if   [[ -z "${ICCG_OUT_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} ICCG_OUT_DIR not defined, exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 13
elif [[ ! -d "${ICCG_OUT_DIR}" ]] ; then
  for CMD in \
    "mkdir -p ${ICCG_OUT_DIR}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${ICCG_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${ICCG_LOG_FP}"
    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${ICCG_LOG_FP}"
      exit 14
    fi
  done

  # sooo ... did it work?
  if [[ ! -d "${ICCG_OUT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} could not create iccg output dir='${ICCG_OUT_DIR}', exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
    exit 15
  fi
fi # [[ ! -d "${ICCG_OUT_DIR}" ]]

### Handle (broken/downlevel R package=raster || AMAD filenaming conventions):
### if METCRO2D_FP does not have a standard extension, *copy* to one that does--
### note symlinking is not good enough for package=raster!
METCRO2D_FN="$(basename ${METCRO2D_FP})"
METCRO2D_EXT="${METCRO2D_FN##*.}"
if   [[ -z "${METCRO2D_EXT}" ]] ; then
  echo -e "${ERROR_PREFIX} METCRO2D_EXT not defined, exiting ..." 2>&1 | tee -a "${ICCG_LOG_FP}"
  exit 16
# note: 'nc' is the only standard extension per http://www.unidata.ucar.edu/software/netcdf/docs/faq.html#filename
elif [[ "${METCRO2D_EXT}" != 'nc' ]] ; then

  METCRO2D_FP_OLD="${METCRO2D_FP}"
  METCRO2D_FP="${METCRO2D_FP}.nc"
  echo # newline
  for CMD in "cp ${METCRO2D_FP_OLD} ${METCRO2D_FP}" ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${ICCG_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${ICCG_LOG_FP}"
  #  if [[ $? -ne 0 ]] ; then
    # $? only works for end of pipeline: use PIPESTATUS array to get exit status of each pipeline command
    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${ICCG_LOG_FP}"
      exit 17
    fi
    # TODO: check pipeline output from CMD
  done
  echo # newline

fi # [[ -z "${METCRO2D_EXT}" ]]

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### Run main R script. Note order of arguments: until our R arg-parsing improves, must keep this order.

MAKE_ICCG_START="$(date)"
echo -e "start making ICCG=${MAKE_ICCG_START}"

for CMD in \
  "${R_RUNNER} ${ICCG_BUILD_R}\
   METCRO2D_fp='${METCRO2D_FP}'\
   METCRO2D_var_name='${METCRO2D_VAR_NAME}' \
   METCRO2D_len_unit='${METCRO2D_LEN_UNIT}'\
   iccg_in_fp='${ICCG_IN_FP}'\
   iccg_out_fp='${ICCG_OUT_FP}'"\
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${ICCG_LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${ICCG_LOG_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${ICCG_LOG_FP}"
    exit 18
  fi
  # TODO: check pipeline output from CMD
done
echo # newline

### Cleanup, lookaround

## Handle (broken/downlevel R package=raster || AMAD filenaming conventions), more scarily :-(
if [[ -r "${METCRO2D_FP_OLD}" && -r "${METCRO2D_FP}" ]] ; then
  for CMD in \
    "rm ${METCRO2D_FP}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${ICCG_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${ICCG_LOG_FP}"
    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${ICCG_LOG_FP}"
      exit 19
    fi
  done
  echo # newline
  METCRO2D_FP="${METCRO2D_FP_OLD}"
fi

for CMD in \
  "ls -al ${METCRO2D_FP}*" \
  "ls -al ${ICCG_OUT_FP}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${ICCG_LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${ICCG_LOG_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${ICCG_LOG_FP}"
    exit 20
  fi
done
echo # newline

echo -e "  end making ICCG=$(date)" 2>&1 | tee -a "${ICCG_LOG_FP}"
echo -e "start making ICCG=${MAKE_ICCG_START}" 2>&1 | tee -a "${ICCG_LOG_FP}"

exit 0
