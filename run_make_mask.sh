#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Setup and run make_mask.R (derived from ocean_mask.R).
### Note R will require packages={ncdf4, M3}.
### Should not be necessary when run from ./Makefiles/*, except that
### downlevel R package=raster (e.g., version='2.0-12 (1-September-2012)' on HPCC) don't support netCDF with non-standard file extensions

### Much code in common with run_{make, plot}_{ICCG, mask}.sh. TODO: refactor!

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### TODO: non-positional argument parsing!

THIS="$0"
METCRO2D_FP="$1"
METCRO2D_LEN_UNIT="$2"
MASK_OUT_FP="$3"
R_RUNNER="$4"
MASK_BUILD_R="$5"

### For logging

THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
MASK_LOG_FP="${THIS_DIR}/${THIS_FN}.log"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

### Test args, fail fast on error

if   [[ -z "${MASK_LOG_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} MASK_LOG_FP not defined, exiting ..."
  exit 1
elif [[ -z "${METCRO2D_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} METCRO2D_FP not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 2
elif [[ ! -r "${METCRO2D_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} cannot read grid input='${METCRO2D_FP}', exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 3
elif [[ -z "${METCRO2D_LEN_UNIT}" ]] ; then
  echo -e "${ERROR_PREFIX} METCRO2D_LEN_UNIT not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 4
elif [[ -z "${MASK_OUT_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} MASK_OUT_FP not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 7
elif [[ -r "${MASK_OUT_FP}" && ! -w "${MASK_OUT_FP}" ]] ; then
!  echo -e "${ERROR_PREFIX} MASK output='${MASK_OUT_FP}' can be read but not written, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 8
elif [[ -z "${R_RUNNER}" ]] ; then
  echo -e "${ERROR_PREFIX} R_RUNNER not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 13
elif [[ -z "${MASK_BUILD_R}" ]] ; then
  echo -e "${ERROR_PREFIX} MASK_BUILD_R not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 14
elif [[ ! -r "${MASK_BUILD_R}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read main R file='${MASK_BUILD_R}', exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 15
fi # [[ -z "${MASK_LOG_FP}" ]]

### Ensure dir/folder for output

MASK_OUT_DIR="$(dirname ${MASK_OUT_FP})"
if   [[ -z "${MASK_OUT_DIR}" ]] ; then
  echo -e "${ERROR_PREFIX} MASK_OUT_DIR not defined, exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
  exit 5
elif [[ ! -d "${MASK_OUT_DIR}" ]] ; then
  for CMD in \
    "mkdir -p ${MASK_OUT_DIR}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${MASK_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${MASK_LOG_FP}"
    if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${MASK_LOG_FP}"
      exit 6
    fi
  done

  # sooo ... did it work?
  if [[ ! -d "${MASK_OUT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} could not create mask output dir='${MASK_OUT_DIR}', exiting ..." 2>&1 | tee -a "${MASK_LOG_FP}"
    exit 7
  fi
fi # [[ ! -d "${MASK_OUT_DIR}" ]]

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### Run main R script. Note order of arguments: until our R arg-parsing improves, must keep this order.

MAKE_MASK_START="$(date)"
echo -e "mask creation start=${MAKE_MASK_START}"

for CMD in \
  "${R_RUNNER} ${MASK_BUILD_R}\
   METCRO2D_fp='${METCRO2D_FP}'\
   METCRO2D_len_unit='${METCRO2D_LEN_UNIT}'\
   mask_out_fp='${MASK_OUT_FP}'" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${MASK_LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${MASK_LOG_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${MASK_LOG_FP}"
    exit 16
  fi
  # TODO: check pipeline output from CMD
done
echo # newline

### Cleanup, lookaround

for CMD in \
  "ls -al ${MASK_OUT_FP}" \
  "ls -al ${MASK_LOG_FP}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${MASK_LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${MASK_LOG_FP}"
  if [[ "${PIPESTATUS[0]}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${MASK_LOG_FP}"
    exit 17
  fi
done
echo # newline

echo -e "mask creation   end=$(date)" 2>&1 | tee -a "${MASK_LOG_FP}"
echo -e "mask creation start=${MAKE_MASK_START}" 2>&1 | tee -a "${MASK_LOG_FP}"

exit 0
